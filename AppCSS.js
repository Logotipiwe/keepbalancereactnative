const styles = {
    background:{
        flex: 1,
        width: '100%',
        height: '100%',
    },
    app: {
        flex: 1,
    },
    tabNav:{
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        borderTopColor: 'black',
        borderTopWidth: 1,
        borderTopStyle: 'solid',
        height: 50,

        item:{
            width: 70,
            justifyContent: 'center',
            alignItems: 'center',

            svg:{
                fill: 'rgb(140,140,140)',
                height: 28,
                width: 28,
            }
        }
    }
};
module.exports = styles;
