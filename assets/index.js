import Alko     from './img/alko.svg'
import Cafe     from './img/cafe.svg'
import Clothes  from './img/clothes.svg'
import Dolg     from './img/dolg.svg'
import Drink    from './img/drink.svg'
import Food     from './img/food.svg'
import Found    from './img/found.svg'
import Gift     from './img/gift.svg'
import Hint     from './img/hint.svg'
import Hoz      from './img/hoz.svg'
import Income   from './img/income.svg'
import Invest   from './img/invest.svg'
import Out      from './img/out.svg'
import Question from './img/question.svg'
import Razv     from './img/razv.svg'
import Salary   from './img/salary.svg'
import Service  from './img/service.svg'
import Store    from './img/store.svg'
import Svyaz    from './img/svyaz.svg'
import Sweet    from './img/sweet.svg'
import Transfer from './img/transfer.svg'

const Test = Salary;
const Test2 = Alko;

export {Test}
export {Test2}
export {Alko}
export {Cafe}
export {Clothes}
export {Dolg}
export {Drink}
export {Food}
export {Found}
export {Gift}
export {Hint}
export {Hoz}
export {Income}
export {Invest}
export {Out}
export {Question}
export {Razv}
export {Salary}
export {Service}
export {Store}
export {Svyaz}
export {Sweet}
export {Transfer}
