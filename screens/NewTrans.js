import React from 'react';
import {View, Text, BackHandler, TextInput, Picker, TouchableOpacity} from 'react-native';
import styles from './NewTransCSS'
import BackSvg from './../assets/left-arrow.svg'

class NewTrans extends React.Component {
    constructor(props) {
        super(props);
        const initState = this.props.initState ? this.props.initState : {};

        this.state = Object.assign({},{
            selectedWalletId: this.props.wallets
        },initState)
    }


    componentDidMount(): void {
        this.BackHandler = BackHandler.addEventListener('hardwareBackPress',()=>{
            this.props.goBack();
            return true;
        })
    }

    componentWillUnmount(): void {
        this.BackHandler.remove();
    }

    render() {
        let appData = this.props.appData;

        return (
            <View style={{height: '100%'}}>
                <View style={styles.header}>
                    <BackSvg height={20} width={20} onPress={this.props.goBack}/>
                    <Text style={styles.header.text}>Новая транзакция</Text>
                </View>
                <View style={styles.menu}>
                    <TextInput style={styles.valueInput} keyboardType={'numeric'}/>
                    <Picker>
                        {appData.wallets.map(wallet=>{
                            return (
                                <Picker.Item label={wallet.title} value={wallet.id} key={wallet.id}/>
                            )
                        })}
                    </Picker>
                </View>
            </View>
        );
    }
}

export default NewTrans;
