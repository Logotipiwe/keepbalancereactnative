import {NativeModules} from 'react-native';

const {StatusBarManager} = NativeModules;
const styles = {
    header: {
        flexDirection: 'row',
        marginTop: StatusBarManager.HEIGHT + 10,
        marginLeft: 20,
        alignItems: 'center',

        text: {
            marginLeft: 10,
            fontSize: 25,
        },
    },
    menu: {
        paddingTop: 30,
        backgroundColor: 'white',
        margin: '3%',
        flex: 1,
        borderRadius: 10,
        boxSizing: 'border-box',
    },
    valueInput:{

    }
};
module.exports = styles;
