/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {View, ImageBackground, Text, TextInput, Button} from 'react-native';

export default class Login extends Component{
    render() {
        return (
            <View style={styles.form}>
                <Text style={styles.h1} >Регистрация</Text>
                <TextInput
                    autoCompleteType='username'
                    placeholder='Логин..'
                    style={styles.input}
                />
                <TextInput
                    autoCompleteType='password'
                    secureTextEntry={true}
                    placeholder='Пароль..'
                    style={styles.input}
                />
                <View style={styles.buttonView}>
                    <Button
                        title='Зарегистрироваться'
                        style={styles.button}
                        onPress={()=>{}}/>
                </View>
                <Text style={styles.text}>
                    Уже зарегистрированы?  <Text style={styles.link} onPress={this.props.onLink}>Вам сюда</Text>
                </Text>
            </View>
        )
    }
}

const styles = {
    form: {
        alignSelf: 'center',
        marginTop: 200,
        borderRadius: 10,
        alignItems: 'center',
        height: 300,
        width: '100%',
        paddingHorizontal: '10%',
        flexDirection: 'column'
    },
    h1: {
        fontSize: 30,
    },
    input: {
        width: '100%',
        backgroundColor: 'rgba(232, 240, 254,0.9)',
        borderRadius: 10,
        height: 40,
        paddingLeft: 10,
        marginVertical: 10
    },
    buttonView: {
        width: '100%',
        marginVertical: 5
    },
    text:{
        marginVertical: 30,
        fontSize: 16
    },
    link:{
        color: 'wheat',
        textDecorationLine: 'underline'
    }
};
