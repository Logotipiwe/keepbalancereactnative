import {NativeModules} from 'react-native';

const {StatusBarManager} = NativeModules;
const styles = {
    header: {
        flexDirection: 'row',
        marginTop: StatusBarManager.HEIGHT + 10,
        marginLeft: 20,
        alignItems: 'center',

        text: {
            marginLeft: 10,
            fontSize: 30,
        },
    },
    menu: {
        paddingTop: 30,
        backgroundColor: 'white',
        margin: '3%',
        flex: 1,
        borderRadius: 10,
        boxSizing: 'border-box',

        item: {
            marginHorizontal: 20,
            paddingVertical: 10,
            flexDirection: 'row',
            borderBottomColor: 'lightgray',
            borderBottomWidth: 1,
            borderBottomStyle: 'solid',
            justifyContent: 'space-between',
            text: {
                fontSize: 20,
            },
            input: {
                margin: 0,
                width: 84,
                height: 20,
                padding: 0,
            },
        },
    },
};
module.exports = styles;
