import React from 'react';
import {View, Text, BackHandler, Picker, TouchableOpacity} from 'react-native';
import styles from './MenuCSS';
import BackSvg from './../assets/left-arrow.svg'

class Menu extends React.Component {
    componentDidMount(): void {
        this.BackHandler = BackHandler.addEventListener('hardwareBackPress',()=>{
            this.props.goBack();
            return true;
        })
    }

    componentWillUnmount(): void {
        this.BackHandler.remove();
    }

    render() {
        const p = this.props;
        const lastDayNum = new Date(
            p.currDate.getFullYear(),
            p.currDate.getMonth() + 1,
            0,
        ).getDate();
        return (
            <View style={{height: '100%'}}>
                <View style={styles.header}>
                    <BackSvg height={20} width={20} onPress={this.props.goBack}/>
                    <Text style={styles.header.text}>Меню</Text>
                </View>
                <View style={styles.menu}>
                    <View style={styles.menu.item}>
                        <Text style={styles.menu.item.text}>Первый день</Text>
                        <Picker
                            style={styles.menu.item.input}
                            onValueChange={p.changeStartDay} //unhandled ставить дату сразу а не по callback
                            selectedValue={p.startDay}
                        >
                            {Array(lastDayNum).fill(null).map((x, i) => {
                                let day = i+1;
                                return (
                                    <Picker.Item
                                        key={day}
                                        label={day.toString()}
                                        value={day}
                                    />
                                );
                            })}
                        </Picker>
                    </View>
                    <TouchableOpacity style={styles.menu.item} onPress={this.props.logOutSubmit}>
                        <Text style={[styles.menu.item.text, {color: 'red'}]}>Выйти</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

export default Menu;
