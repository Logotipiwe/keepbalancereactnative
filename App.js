/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {NativeModules, StatusBar, View, Text, Image, ImageBackground, ScrollView,
    TouchableOpacity, RefreshControl} from 'react-native';
import styles from './AppCSS';
import HomeSVG from './assets/home.svg';
import BurgerSVG from './assets/burger.svg';
import NewTransSVG from './assets/newTrans.svg'
import Main from './App/Main';
import Header from './App/Header';
import Login from './screens/Login';
import Registration from './screens/Registration';
import Menu from './screens/Menu';
import NewTrans from './screens/NewTrans';

const {StatusBarManager} = NativeModules;

class App extends Component {
    constructor(props) {
        super(props);
        const url = 'https://logotipiwe.ru/new_money_test/back/api.php';
        this.state = {
            currDate: new Date(),
            appData: {},
            menuScreen: 'newTrans',
            url,
            initialized: false,
            is404: false,
            refreshing: false,
            isRegistration: false,
            debug: false,
        };
        this.fetchData();
    }

    doAjax = (url, data, method = 'GET', options = {}, success = () => this.fetchData()) => {
        if (options === undefined && method === 'GET') {
            options = {
                dataType: 'json',
            };
        }

        if (this.state.debug) {
            data.debug = 'hduh43yh5u43ij4tj43jy';
        }

        let requestUrl = url;

        if (method !== 'GET') {
            options.method = method;
            options.body = JSON.stringify(data);
            options.headers = {
                Accept: 'application/json', 'Content-Type': 'application/json',
            };
        } else {
            requestUrl = url + this.objToGet(data);
        }

        return fetch(requestUrl, options).then(x => {
            console.log('-------------REQUEST----------');
            console.log(requestUrl);
            console.log('OPTIONS: ' + JSON.stringify(options));
            console.log('ANSWER: ' + JSON.stringify(x));
            return x.json();
        }).then(res => {
            console.log('RESPONSE: ' + JSON.stringify(res));
            success(res);
        });
    };

    intInput = val => {
        return val.replace(/[^0-9+-]/g, '');
    };

    calculate = str => {
        if (isFinite(str)) {
            return parseInt(str);
        }
        const onlyNums = str.replace(/[+-]/g, '');
        if (!isFinite(onlyNums)) {
            return NaN;
        }

        let arr = str.split('+').map(x => {
            if (isFinite(x)) {
                return parseInt(x);
            }
            return x.split('-').map((x2, i2) => {
                x2 = parseInt(x2);
                if (i2 > 0) {
                    return -x2;
                } else {
                    return x2;
                }
            });
        });
        let ret = [];
        arr.forEach(x => {
            if (isFinite(x)) {
                ret.push(x);
            } else {
                x.forEach(x2 => ret.push(x2));
            }
        });

        return ret.reduce((sum, num) => sum + num, 0);
    };

    objToGet = get_data => {
        let get = [];
        if (Object.keys(get_data).length) {
            for (let key in get_data) {
                get.push(key + '=' + get_data[key]);
            }
            get = '?' + get.join('&');
        } else {
            get = '';
        }
        return get;
    };

    getCurrDate = () => {
        let currDate = this.state.currDate;
        let date_str = currDate.getFullYear() + '-' + (currDate.getMonth() + 1) + '-' + currDate.getDate();
        return date_str;
    };

    fetchData = (date_str = this.getCurrDate()) => {
        let get = {
            method: 'data_get',
            date: date_str,
        };
        return this.doAjax(this.state.url, get, 'GET', undefined, res => {
            if (res.ok) {
                this.setState(s => {
                    let newState = Object.assign({}, s);
                    newState.appData = res.ans;
                    newState.initialized = true;
                    newState.is404 = false;
                    return newState;
                });
            } else {
                if (res.err === 'auth_err') {
                    this.setState(s => {
                        let newState = Object.assign({}, s);
                        newState.is404 = true;
                        newState.initialized = true;
                        return newState;
                    });
                }
            }
        });
    };

    changeStartDay = val => {
        const get = {
            method: 'change_start_day',
            date: this.getCurrDate(),
            val,
        };
        this.doAjax(this.state.url, get);
    };

    newWallet = title => {
        if (title === '') {
            return 0;
        }

        const get = {
            method: 'wallet_new',
            title,
        };
        this.doAjax(this.state.url, get);
    };

    initWallet = e => {
        const value = this.calculate(e.target.value);
        let get = {
            method: 'wallet_init',
            date: this.getCurrDate(),
            value,
            wallet_id: e.target.parentElement.dataset.id,
        };
        this.doAjax(this.state.url, get);
    };

    changeWalletInit = (wallet_id, e) => {
        let value = e.nativeEvent.text;
        if (value === '') {
            value = 0;
        }

        let get = {
            method: 'wallet_init', wallet_id, value,
            date: this.getCurrDate(),
        };
        this.doAjax(this.state.url, get);
    };

    changeWalletValueInput = (e) => {
        e.persist();
        let val = this.intInput(e.target.value);
        this.setState(s => {
            let newState = Object.assign({}, s);
            newState.appData.wallets[$(e.target.parentElement).index()].valueInput = val;
            return newState;
        });
    };

    changeWalletValue = (wallet_num, e) => {
        let val = e.nativeEvent.text;

        let wallet = this.state.appData.wallets[wallet_num];
        let diff = val - this.calculate(wallet.value);
        if (diff === 0) {
            return;
        }
        let type = 3;
        if (diff < 0) {
            type = 1;
            diff = -diff;
        }
        const get = {
            method: 'transaction_new',
            wallet_id: wallet.id,
            type,
            value: diff,
            date: this.getCurrDate(),
        };
        this.doAjax(this.state.url, get);
    };

    delWallet = id => {
        let get = {
            method: 'wallet_del',
            wallet_id: id,
        };
        this.doAjax(this.state.url, get);
    };

    tagNew = tag_name => {
        let data = {
            method: 'tag_new',
            tag_name,

        };
        let res = this.doAjax(this.state.url, data);
    };

    newTransaction = (value, type, wallet_id, to_wallet, category, tags_ids) => {
        let data = {
            method: 'transaction_new',
            value, type, wallet_id,
            date: this.getCurrDate(),
        };
        if (to_wallet !== null) {
            data.to_wallet = to_wallet;
        }
        if (category !== null) {
            data.category = category;
        }
        if (tags_ids !== null) {
            data.tags_ids = tags_ids;
        }

        this.doAjax(this.state.url, data, 'POST');
    };

    delTransaction = id => {
        let get = {
            method: 'transaction_del',
            id,
        };
        this.doAjax(this.state.url, get);
    };

    currDateChange = (val) => {
        this.setState(s => {
            let newState = Object.assign({}, s);
            newState.currDate = new Date(newState.currDate.setDate(newState.currDate.getDate() + val));
            this.fetchData(this.getCurrDate(newState.currDate));
            return newState;
        });

    };

    dateHuman = (date, short = 0) => {
        const months = ['января', 'феваля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сенября', 'октября', 'ноября', 'декабря'];
        const months_short = ['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'];
        date = new Date(date);
        let out_arr = (short) ? months_short : months;
        return date.getDate() + ' ' + out_arr[date.getMonth()];
    };

    onRefresh = ()=>{
        this.setState({refreshing: true});
        this.fetchData().then(res=>this.setState({refreshing: false}));
    };

    menuClick = title=>{
        this.setState(s => {
            let newState = Object.assign({}, s);
            newState.menuScreen = title;
            return newState;
        });
    };
    goBack = this.menuClick.bind(null,'home');

    changeRegistration = () => {
        this.setState(s => {
            let newState = Object.assign({}, s);
            newState.isRegistration = !newState.isRegistration;
            return newState;
        });
    };

    signInSubmit = (login, password) => {
        let method = 'sign_in';
        this.doAjax(this.state.url, {login, password, method}, 'GET', undefined, res => {
            this.fetchData();
        });
    };

    logOutSubmit = () => {
        console.log('hehe');
        let method = 'log_out';
        this.doAjax(this.state.url, {method}, 'GET', undefined, () => this.fetchData());
    };

    render() {
        let screens = {
            home: {
                jsx: <ScrollView refreshControl={
                    <RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefresh}/>
                }>
                    <Header
                        currDate={this.state.currDate}
                        isInit={this.state.appData.is_init}
                        dateHuman={this.dateHuman}
                        currDateChange={this.currDateChange}
                        startDay={this.state.appData.start_day}
                        intInput={this.intInput}
                    />
                    <Main
                        intInput={this.intInput}
                        appData={this.state.appData}
                        dateHuman={this.dateHuman}
                        newWallet={this.newWallet}
                        tagNew={this.tagNew}
                        initWallet={this.initWallet}
                        changeWalletInitInput={this.changeWalletInitInput}
                        changeWalletInit={this.changeWalletInit}
                        changeWalletValueInput={this.changeWalletValueInput}
                        changeWalletValue={this.changeWalletValue}
                        delWallet={this.delWallet}
                        newTrans={this.newTransaction}
                        delTrans={this.delTransaction}
                    />
                </ScrollView>,
                menu: {
                    svg: HomeSVG,
                    title: 'Главная',
                },
            },
            newTrans: {
                jsx: <NewTrans
                    goBack={this.goBack}
                    appData={this.state.appData}
                />,
                menu: {
                    svg: NewTransSVG,
                    title: "+транз"
                },
                hide: (this.state.initialized) ? !this.state.appData.wallets.length : false
            },
            menu: {
                jsx: <Menu
                    goBack={this.goBack}
                    startDay={this.state.appData.start_day}
                    changeStartDay={this.changeStartDay}
                    currDate={this.state.currDate}
                    logOutSubmit={this.logOutSubmit}
                />,
                menu: {
                    svg: BurgerSVG,
                    title: 'Меню',
                },
            },
        };
        let screen = '';
        if (!this.state.is404) {
            screen = screens[this.state.menuScreen].jsx;
        } else {
            screen = (!this.state.isRegistration) ?
                (<Login
                    onLink={this.changeRegistration}
                    onSubmit={this.signInSubmit}
                />) :
                (<Registration onLink={this.changeRegistration}/>);
        }
        if (this.state.initialized) {
            return (
                <ImageBackground
                    source={require('./assets/fon.png')}
                    style={styles.background}
                >
                    <StatusBar barStyle={'light-content'} backgroundColor={'transparent'} translucent={true}/>
                    <View style={styles.app}>
                        {screen}
                    </View>
                    <View style={[styles.tabNav,{display: (this.state.is404) ? 'none' : 'flex'}]}>
                        {Object.keys(screens).filter(title=>!screens[title].hide).map((screen_title) => {
                            let screen = screens[screen_title];
                            let svgStyle = styles.tabNav.item.svg;
                            if(screen_title===this.state.menuScreen) svgStyle = Object.assign({},svgStyle,{fill:'black'});
                            return (
                                <TouchableOpacity key={screen_title} style={styles.tabNav.item} onPress={this.menuClick.bind(null,screen_title)}>
                                    {screen.menu.svg(svgStyle)}
                                    <Text>{screen.menu.title}</Text>
                                </TouchableOpacity>
                            );
                        })}
                    </View>
                </ImageBackground>
            );
        } else {
            return (<View><Text>Cerf ,kznm</Text></View>);
        }
    }
}

export default App;
