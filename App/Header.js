import React from 'react'
import { Text, View, TextInput } from 'react-native';
import styles from './HeaderCSS'
import CurrDate from "./Header/CurrDate";

class Header extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            startDay: props.startDay
        }
    }


    startDayInp = e=>{
        e.persist();
        e.target.value = this.props.intInput(e.target.value);
    };

    startDayBlur = e=>{
        if(e.target.value !== this.state.startDay){
            this.props.changeStartDay(e);
        }
    };

    render() {
        return (
            <View style={styles.header}>
                <CurrDate
                    currDate={this.props.currDate}
                    dateHuman={this.props.dateHuman}
                    currDateChange={this.props.currDateChange}
                />
            </View>
        )
    }
}
export default Header
