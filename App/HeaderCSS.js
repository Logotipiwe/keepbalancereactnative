import {NativeModules} from 'react-native'
const {StatusBarManager} = NativeModules;
const styles = {
    header: {
        backgroundColor: 'rgba(0,0,0,0.3)',
        paddingTop: StatusBarManager.HEIGHT,
        height: 40+StatusBarManager.HEIGHT,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    startDay: {
        color: "wheat",
        backgroundColor: 'rgba(0,0,0,0.2)',
        textAlign: 'center',
        borderRadius: 5,
        fontSize: 19,
        marginHorizontal: 15,
        paddingHorizontal: 3
    },
    logoutDiv:{
        flexGrow: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    logout: {
        fontSize: 22,
        marginRight: 10,
        color: 'wheat'
    }
};
module.exports = styles;
