import React from 'react'
import {Text, View, Button} from "react-native";
import styles from './CurrDateCSS';

class CurrDate extends React.Component{
    render() {

        let date_str = this.props.dateHuman(this.props.currDate);
        return (
            <View id="CurrDate" style={styles.currDate}>
                <Text style={styles.text}><Text style={styles.text}>{date_str.split(' ')[0]}</Text> {date_str.split(' ')[1]}</Text>
                <Text style={styles.button} title='<' onPress={this.props.currDateChange.bind(null,-1)/*unhandled загрузка данных*/}>{'<'}</Text>
                <Text style={styles.button} title='>' onPress={this.props.currDateChange.bind(null,1)}>></Text>
            </View>
        )
    }
}
export default CurrDate
