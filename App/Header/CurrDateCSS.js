const styles ={
    currDate: {
        marginLeft: 10,
        flexDirection: 'row',
        height: '100%',
        alignItems: 'center'
    },
    text: {
        marginRight: 10,
        fontSize: 25,
        color: 'wheat'
    },
    button:{
        height: '100%',
        width: 30,
        paddingTop: 5,
        paddingLeft: 10,
        backgroundColor: 'rgba(0,0,0,0.1)',
        fontSize: 20,
        color: 'wheat',
        paddingHorizontal: 5,
        marginHorizontal: 2
    }
};
module.exports = styles;
