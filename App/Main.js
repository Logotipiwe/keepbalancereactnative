import React from 'react'
import { Text, View } from 'react-native';
import Wallets from "./Main/Wallets";
import styles from './MainCSS'
import Transactions from "./Main/Transactions";
import Analytics from '../App/Main/Analytics';

class Main extends React.Component{
    render() {
        return (
            <View style={styles.main}>
                <Analytics
                    isInit={(this.props.appData.is_init)}
                    analytics={this.props.appData.analytics}
                    balances={this.props.appData.balances}
                    dateHuman={this.props.dateHuman}
                />
                <Wallets
                    wallets={this.props.appData.wallets}
                    initWallet={this.props.initWallet}
                    changeWalletValueInput={this.props.changeWalletValueInput}
                    changeWalletValue={this.props.changeWalletValue}
                />
                <Transactions
                    isInit={(this.props.appData.is_init)}
                    // intInput={this.props.intInput}
                    transactions={this.props.appData.transactions}
                    types={this.props.appData.transaction_types}
                    // tagNew={this.props.tagNew}
                    tags={this.props.appData.tags}
                    categories={this.props.appData.categories}
                    wallets={this.props.appData.wallets}
                    // newTrans={this.props.newTrans}
                    delTrans={this.props.delTrans}
                />
            </View>
        )
    }
}
export default Main
