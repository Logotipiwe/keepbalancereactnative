const styles = {
    wallets:{
        backgroundColor: 'white',
        margin: '3%',
        borderRadius: 10,
        boxSizing: 'border-box',
    },
    header: {
        textAlign: 'center',
        borderBottomColor: 'gray',
        borderBottomWidth: 1,
        marginHorizontal: 7
    },
    walletNew:{
        flexDirection: 'row',
        height: 30,
        borderBottomColor: 'black',
        borderBottomWidth: 1
    },
    walletNewInp:{
        flex: 1,
        paddingVertical: 0,
        paddingLeft: 10
    },
    walletNewButton:{
        padding: 0,
        alignItems: 'flex-start',
        fontSize: 10,
        marginHorizontal: 5,
        borderRadius: 10
    },
    walletItem:{
        flexDirection: 'row',
        alignItems: 'center',
        height: 40,
        del:{
            transform: [{rotate: '45deg'}],
            marginHorizontal: 5,
            fontSize: 20
        },
        title: {
            fontSize: 22,
            marginHorizontal: 10,
            flexGrow: 1
        },
        valueInit: {
            color: 'lightgray',
            borderBottomColor: 'lightgray'
        },
        value: {
            fontSize: 22,
            marginHorizontal: 20,
            paddingVertical: 0,
            width: 75,
            borderBottomColor: 'black',
            borderBottomWidth: 1,
            textAlign: 'right'
        }
    }
};
module.exports = styles;
