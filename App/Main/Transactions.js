import React from 'react'
import {Text, View, TouchableOpacity} from "react-native";
import styles from './TransactionsCSS'
import * as Svgs from './../../assets'

class Transactions extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            selectedValue: '',
            selectedWalletId: (props.wallets.length)?props.wallets[0].id:0,
            selectedToWallet: null,
            selectedType: 1,
            selectedCat: null,
            selectedTags: [],
            showCats: false,
            inputTag: null
        }
    }

    render() {
        let transactions = this.props.transactions;
        let types = this.props.types;
        let wallets = this.props.wallets;

        if(this.props.isInit) {
            return (
                <View style={styles.transactions} >
                    <View>
                        <Text style={styles.header}>ТРАНЗАКЦИИ</Text>
                    </View>
                    <View className="list">
                        {transactions && transactions.map((transaction, i) => {
                            let color = 'black';
                            switch (transaction.type) {
                                case 1:
                                    color = '#000000';
                                    break;
                                case 4:
                                    color = '#139d00';
                                    break;
                                default:
                                    break;
                            }
                            //название счёта
                            let title = '';
                            if (transaction.type === 2) {
                                title = transaction.title + ' -> ' + transaction.to_title;
                            } else if (transaction.type === 4) {
                                title = this.props.types[3].title;
                            } else {
                                title = transaction.title;
                            }
                            let value_pref = '';
                            if (transaction.type === 2) value_pref = '->';
                            if (transaction.type === 3) value_pref = '+';
                            if (transaction.type === 5) value_pref = '-';

                            let category = (transaction.category) ? this.props.categories[transaction.category] : null;

                            let catImgBackgroundColor = (category) ? category.color : 'rgb(185,185,185)';

                            let svgPath = (category) ? category.img : types.filter(x=>x.id===transaction.type)[0].img;
                            svgPath = svgPath.split('/').reverse()[0].split('.')[0];
                            svgPath = svgPath[0].toUpperCase() + svgPath.substr(1);
                            let catSvg = Svgs[svgPath](styles.transItem.catImgSvg);
                            return (
                                <View style={styles.transItem} key={i}>
                                    <TouchableOpacity style={styles.transItem.del}  onPress={this.props.delTrans.bind(null, transaction.id)}>
                                        <Text style={styles.transItem.delImg}>+</Text>
                                    </TouchableOpacity>
                                    <View style={[styles.transItem.catImg,{backgroundColor: catImgBackgroundColor}]}>
                                        {catSvg}
                                    </View>
                                    <View style={styles.transItem.info} >
                                        <Text
                                            className="category"
                                            style={[styles.transItem.info.categoryTitle,{color: (category)?'black':'gray'}]}
                                        >
                                            {((category) ? category.title : types.filter(x=>x.id===transaction.type)[0].title)}</Text>
                                        <Text style={styles.transItem.info.fromTitle}>{title}</Text>
                                    </View>
                                    <View style={styles.transItem.tags}>
                                        {transaction.tags_ids.map(tag_id => {
                                            let tag_name = this.props.tags[tag_id];
                                            return (
                                                <Text style={styles.transItem.tags.item} key={tag_id}>{tag_name}</Text>
                                            )
                                        })}
                                    </View>
                                    <Text  style={[styles.transItem.value,{color}]}>
                                        {value_pref + transaction.value}
                                    </Text>
                                </View>
                            )
                        })}
                    </View>
                </View>
            )
        } else {
            return <View/>;
        }
    }
}
export default Transactions
