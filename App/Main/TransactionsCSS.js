let styles = {
    transactions: {
        backgroundColor: 'white',
        margin: '3%',
        borderRadius: 10,
        boxSizing: 'border-box',
    },
    header: {
        textAlign: 'center',
        borderBottomColor: 'gray',
        borderBottomWidth: 1,
        marginHorizontal: 7,
    },
    newTrans: {
        firstRow: {
            flexDirection: 'row',
            transInp: {
                flex: 1,
                paddingLeft: 10,
            },
        },
    },
    transItem: {
        flexDirection: 'row',
        alignItems: 'center',
        del:{
            height: '100%',
            paddingTop: 4,
            paddingLeft: 6,
        },
        delImg: {
            color: 'gray',
            transform: [{rotate: '45deg'}],
            marginHorizontal: 0,
            fontSize: 22,
            height: 23
        },
        catImg: {
            height: 34,
            width: 34,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'red',
            borderRadius: 17,
        },
        catImgSvg: {
            height: '70%',
            width: '70%',
            fill: 'black', //SVG only!
        },
        info: {
            marginLeft: 10,
            categoryTitle: {
                fontWeight: '700',
            },
            fromTitle:{
                color:'green'
            }
        },
        tags:{
            height: '100%',
            flex: 1,
            flexDirection: 'row',
            alignItems: 'flex-end',
            paddingLeft: 20,
            item:{
                marginHorizontal: 4,
                paddingHorizontal: 5,
                borderRadius: 10,
                backgroundColor: 'lightgray',
                alignSelf: 'flex-end',
                height: 20,
            }
        },
        value:{
            marginRight: 10,
            fontSize: 21
        }
    },
};
module.exports = styles;
