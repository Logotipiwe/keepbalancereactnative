import React from 'react'
import {View, Text, Image} from 'react-native'
import styles from './AnalyticsCSS'

class Analytics extends React.Component{
    dateDiffHuman = date=>{
        const inp_date = new Date(date);
        const today = new Date();
        const dateDiff = Math.ceil((inp_date.getTime()-today.getTime())/(3600*24*1000));
        const futureDays = [(<Text style={styles.bold}>Сегодня</Text>),'Завтра','Послезавтра'];
        const pastDays = ['Вчера','Позавчера'];
        if(dateDiff >= 0 && dateDiff < 3) return futureDays[dateDiff];
        if(dateDiff >=3) return dateDiff+' дня';
        if(dateDiff <0 && dateDiff > -3) return pastDays[-dateDiff-1];
        return -dateDiff+' назад'
    }; //человекочитаемая разница в днях

    getColor = val=>{
        if (Math.abs(val)>1000) val=1000*Math.sign(val);
        val = val/1000;
        let red = 0;
        let green = 0;
        if(val>0){
            red = 255*(1-val);
            green = 255;
        } else {
            red = 255;
            green = 255*(1+val);
        }
        return 'rgb(' + red + ',' + green + ',0)';
    };
    render() {
        let balances = this.props.balances;
        if(this.props.isInit) {
            return (
                <View style={styles.analytics}>
                    <View style={styles.header} >
                        <Text>АНАЛИТИКА</Text>
                        {/*<Image src='img/hint.svg' className="hint_img"/>*/}
                    </View>
                    <View style={styles.balances} >
                        {balances && Object.keys(balances).map((date,i) => {
                            let item = balances[date];
                            return (
                                <View style={styles.balances.item}  key={date}>
                                    <Text>{this.dateDiffHuman(date)}</Text>
                                    <Text style={styles.balances.item.date}>{this.props.dateHuman(date)}</Text>
                                    <Text
                                        className='i_value'
                                        style={[styles.balances.item.value(i),{color: this.getColor(item)}]}
                                    >{item}</Text>
                                </View>
                            )
                        })}
                    </View>
                    <View style={styles.data} >
                        <Text className='per_day'>В день: {this.props.analytics.per_day}</Text>
                        <Text className='all'>Всего: {this.props.analytics.value_sum}</Text>
                        <Text className='store'>Стор: {this.props.analytics.store}</Text>
                    </View>
                </View>
            )
        } else return <View/>;
    }
}
export default Analytics
