import React from 'react';
import {Text, View, TextInput, Button} from 'react-native';
import styles from './WalletsCSS';

class Wallets extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            newWalletTitle: '',
            wallets: props.wallets.map(wallet=>{
                wallet.valueInput = wallet.value;
                return wallet;
            })
        };
    }

    static getDerivedStateFromProps(newPr, prState) {
        if(newPr.wallets.filter((prop_wallet,i)=>{
            return (prop_wallet.value !== prState.wallets[i].value)
        }).length){
            prState.wallets = newPr.wallets.map(wallet=>{
                // wallet.initInput = wallet.init;
                wallet.valueInput = wallet.value;
                return wallet;
            });
        }

        return prState;
    }

    inputValue = (wallet_num,e)=>{
        let val = parseInt(e.nativeEvent.text);
        this.setState(s => {
            let newState = Object.assign({}, s);
            newState.wallets[wallet_num].valueInput = (!isNaN(val)) ? val : 0;
            return newState;
        });
    };

    render() {
        let wallets = this.state.wallets;
        return (
            <View style={styles.wallets}>
                <View>
                    <Text style={styles.header}>СЧЕТА</Text>
                </View>
                <View>
                    {wallets && wallets.map((wallet, i) => {
                        return (
                        <View key={i} data-id={wallet.id} style={styles.walletItem}>
                            <Text style={styles.walletItem.title}>{wallet.title}</Text>
                            {(wallet.init) ? <TextInput
                                style={styles.walletItem.value}
                                keyboardType="numeric"
                                onChange={this.inputValue.bind(null,i)}
                                value={(wallet.valueInput!==0) ? wallet.valueInput.toString() : ''}
                                onSubmitEditing={this.props.changeWalletValue.bind(null,i)}
                            /> : <View/>}
                        </View>
                    )})}
                </View>
            </View>
        );
    }
}

export default Wallets;
