const styles = {
    analytics: {
        backgroundColor: 'white',
        margin: '3%',
        borderRadius: 10,
        boxSizing: 'border-box',
    },
    header: {
        width: '100%',
        height: 30,
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0,0,0,0.3)',
        borderBottomStyle: 'solid',
        justifyContent: 'center',
    },
    balances: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: 'gray',
        borderBottomStyle: 'solid',
        justifyContent: 'space-between',

        item: {
            flex: 1,
            textAlign: 'center',
            alignItems: 'center',
            justifyContent: 'center',

            date:{
                color: 'gray',
                fontSize: 10
            },
            value: (i)=>{
                return {
                    fontSize: 30 - 3 * i,
                    fontWeight: '700'

                }
            }
        },
    },
    bold: {
        fontWeight: 'bold',
    },
    data: {
        flexDirection: 'row',
        justifyContent: 'space-around',
    },

};
module.exports = styles;

/*
.balances{
        border-bottom: 1px gray solid;

    .item{
            flex-direction: column;
            flex-grow: 1;

        >div:first-child{
                justify-content: center;
            }
        >div:nth-child(2){
                justify-content: center;
                font-size: 11px;
                color: gray;
                margin: 0;
                padding: 0;
            }
        >div:last-child{
                height: 40px;
                justify-content: center;
                font-size: 30px;
                font-weight: 900;
                align-items: flex-end;
                padding-left: 7px;
            }
        .i_value:after{
                content: '₽';
                font-size: 23px;
                margin-left: 3px;
                margin-bottom: 2px;
            }
        }
    .item:nth-child(2){
        .i_value{
                font-size: 28px;
            }
        }
    .item:nth-child(3){
        .i_value{
                font-size: 26px;
            }
        }
    }
.data{
        font-size: 11px;
        justify-content: space-around;
    }
}
@media (max-width: 1000px) {
    #Analytics{
        order: -2;
        flex-grow: 0;

    .header{
            //display: none;
            height: 20px;
        .title{
                font-size: 11px;
                padding-left: 0;
                justify-content: center;
                font-weight: bolder;
            }
        .hint_img{
                height: 90%;
            }
        }
    }
}*/
